FROM nginx:alpine
COPY site-content /var/www/html
ADD site-conf/default.conf /etc/nginx/conf.d
